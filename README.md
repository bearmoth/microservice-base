## Developing

### Prerequisites

Install `VS Code, Docker Engine` and `Docker Compose. All other dependencies are installed in Docker containers or as VS Code plugins.

When VS Code starts up for the first time it will download the Node docker image used for running development tools such as eslint and Jest. Once that has downloaded, VS Code will spin up a container and install dependencies from the package.json file. You can access the interactive shell within this container with the shortcut `Ctrl+~`;

